#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "../calculator.h"

TEST_CASE("Test operations")
{
        calculator calc;

        SECTION("Addition")
        {
                REQUIRE(calc.add(4, 3) == 7);
        }

        SECTION("Subtraction")
        {
                REQUIRE(calc.subtract(7, 2) == 5);
        }

        SECTION("Multiplication")
        {
                REQUIRE(calc.multiply(2, 8) == 16);
        }
}
